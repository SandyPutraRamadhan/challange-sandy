package com.project.challange.Model;

import javax.persistence.*;

@Entity
// Untuk Create/Membuat Table
@Table(name = "Register")
public class Register {

//    Membuat kolom id dan membuat id menjadi primary key
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    Membuat Kolom Username
    @Column(name = "username")
    private String username;

//    Membuat Kolom Email
    @Column(name = "email")
    private String email;

//    Membuat Kolom Password
    @Column(name = "password")
    private String password;

    public Register() {
    }

    public Register(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
