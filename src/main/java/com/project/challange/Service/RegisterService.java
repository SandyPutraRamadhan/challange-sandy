package com.project.challange.Service;

import com.project.challange.Model.Register;

import java.util.List;
import java.util.Map;

public interface RegisterService {

    Object getRegisterById(Long id);

    Register addRegister(Register register );

    List<Register> getAllRegister();

    Object editRegister(Long id , Register register);

    Map<String, Boolean> deleteRegister(Long id);

    Map<String, Object> login(Register register);
}
