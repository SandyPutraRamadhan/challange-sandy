package com.project.challange.Controller;

import com.project.challange.Model.Register;
import com.project.challange.Service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
//Membuat Request Untuk Client
@RequestMapping("/Register")
@CrossOrigin(origins = "http://localhost:3000")
public class RegisterController {

    @Autowired
    RegisterService registerService;

//  Request  Untuk Memanggil Data per Id
    @GetMapping(path = "/{id}")
    public Object getRegisterById(@PathVariable("id") Long id) {
        return registerService.getRegisterById(id);
    }

//  Request  Untuk Memanggil Semua Data
    @GetMapping
    public List<Register> getAllRegister() {
        return registerService.getAllRegister();
    }
//  Request  Untuk Menambah Data
    @PostMapping
    public Register addRegister(@RequestBody Register register) {
        return registerService.addRegister(register);
    }
//   Request Untuk Meng Edit/ Ubah Data
    @PutMapping(path = "/{id}")
    public Object editRegister(@PathVariable("id") Long id ,@RequestBody Register register) {
        return registerService.editRegister(id, register);
    }
//  Request Untuk Men hapus Data
    @DeleteMapping(path = "/{id}")
    public Object deleteRegister(@PathVariable("id") Long id) {
        return registerService.deleteRegister(id);
    }
    @PostMapping("/sign-in")
    public Map<String, Object> login (@RequestBody Register register) {
        return registerService.login(register);
    }
}
