package com.project.challange.ServiceImpl;

import com.project.challange.Model.Register;
import com.project.challange.Repository.RegisterRepository;
import com.project.challange.Service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    RegisterRepository registerRepository;


    @Override
    public Object getRegisterById(Long id) {
       return registerRepository.findById(id);
    }

    @Override
    public Register addRegister(Register register) {
        return registerRepository.save(register);
    }

    @Override
    public List<Register> getAllRegister() {
        return registerRepository.findAll();
    }

    @Override
    public Object editRegister(Long id, Register register) {
        Register update = registerRepository.findById(id).get();
        update.setUsername(register.getUsername());
        update.setPassword(register.getPassword());
        update.setEmail(register.getEmail());
        return registerRepository.save(update);
    }

    @Override
    public Map<String, Boolean> deleteRegister(Long id) {
            registerRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
    }

    @Override
    public Map<String, Object> login(Register register) {
        Register register1 = registerRepository.findByEmail(register.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("user", register1);
        return response;
    }
}
